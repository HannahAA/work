//constructor for tank
function Tank(volume, pipe) {
    this.volume = volume;
    this.currentVol=0;
    this.pipe=pipe;
};

//constructor for pipe (kinda like a blueprint)
function Pipe(rateOfDischarge, open, close, tK) {
    this.rateOfDischarge = rateOfDischarge;
    this.open=open;
    this.close=close;
    //passing a reference to the tank here
    this.tK = tK;
    //making the variable to which we call setInterval a property of the pipe instead of a global variable
    this.runWater=null;
};

function Controller(tank,pipeList){
    this.tank=tank;
    this.pipeList = pipeList;
    this.tank.isTankFilled().then(()=>{
        this.pipeList.forEach((item) => {
            item.closePipe(item.runWater);
        });
    });
};

//defining a prototype function for pipe
Pipe.prototype.manipulate = function (keyvalue) {
    if (keyvalue === this.open) {
            this.runWater= setInterval(() => {
                this.tK.calculate(this.rateOfDischarge, this);
            }, 1000); 
    } else if (keyvalue === this.close) {
        //clearInterval(this.runWater);
        this.closePipe(this.runWater);
    }
};

Pipe.prototype.closePipe = function (water){
    clearInterval(water);
}

//defining a prototype function for tank
Tank.prototype.calculate = function (amtToAdd, pipe) {
    this.currentVol+=amtToAdd;
    console.log(this.currentVol);

    if (this.currentVol === this.volume) {
        //clearInterval(runWater);
        console.log('Tank is full');
        this.letsdothis();
    } else if (this.currentVol>=this.volume){
        //clearInterval(runWater);
        console.log('Tank is full and has overflowed.');
        //return tempvol;
    }
};

Tank.prototype.isTankFilled=function () {
    return new Promise((resolve)=>{
        this.letsdothis=resolve;
    });
};

const tank1 = new Tank(10);
const pipe1 = new Pipe(1, 'a', 'b', tank1);
const pipe2 = new Pipe(2, 'c', 'd', tank1);
const control1 = new Controller(tank1,[pipe1,pipe2]);

document.addEventListener('keydown', (e) => {
    pipe1.manipulate(e.key);
    pipe2.manipulate(e.key);
});